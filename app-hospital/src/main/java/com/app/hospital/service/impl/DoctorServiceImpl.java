package com.app.hospital.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.hospital.dao.IDoctorDAO;
import com.app.hospital.model.Doctor;
import com.app.hospital.service.IDoctorService;

@Service
public class DoctorServiceImpl implements IDoctorService {

	@Autowired
	private IDoctorDAO dao;

	@Override
	public Doctor registrar(Doctor obj) {
		return dao.save(obj);
	}

	@Override
	public Doctor modificar(Doctor obj) {
		return dao.save(obj);
	}

	@Override
	public List<Doctor> listar() {
		return dao.findAll();
	}

	@Override
	public Doctor leer(Integer id) {
		Optional<Doctor> op = dao.findById(id);
		return op.isPresent() ? op.get() : new Doctor();
	}

	@Override
	public void eliminar(Integer id) {
		dao.deleteById(id);
	}

}
