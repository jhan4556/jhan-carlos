package com.app.hospital.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.hospital.dao.ICityDAO;
import com.app.hospital.model.City;
import com.app.hospital.service.ICityService;

@Service
public class CityServiceImpl implements ICityService {

	@Autowired
	private ICityDAO dao;

	@Override
	public City registrar(City obj) {
		return dao.save(obj);
	}

	@Override
	public City modificar(City obj) {
		return dao.save(obj);
	}

	@Override
	public List<City> listar() {
		return dao.findAll();
	}

	@Override
	public City leer(Integer id) {
		Optional<City> op = dao.findById(id);
		return op.isPresent() ? op.get() : new City();
	}

	@Override
	public void eliminar(Integer id) {
		dao.deleteById(id);
	}

}
