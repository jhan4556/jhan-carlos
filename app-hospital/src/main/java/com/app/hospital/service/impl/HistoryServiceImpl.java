package com.app.hospital.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.hospital.dao.IHistoryDAO;
import com.app.hospital.model.History;
import com.app.hospital.service.IHistoryService;

@Service
public class HistoryServiceImpl implements IHistoryService {

	@Autowired
	private IHistoryDAO dao;

	@Override
	public History registrar(History obj) {
		return dao.save(obj);
	}

	@Override
	public History modificar(History obj) {
		return dao.save(obj);
	}

	@Override
	public List<History> listar() {
		return dao.findAll();
	}

	@Override
	public History leer(Integer id) {
		Optional<History> op = dao.findById(id);
		return op.isPresent() ? op.get() : new History();
	}

	@Override
	public void eliminar(Integer id) {
		dao.deleteById(id);
	}

}
