package com.app.hospital.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.hospital.dao.ISpecialtyDAO;
import com.app.hospital.model.Specialty;
import com.app.hospital.service.ISpecialtyService;

@Service
public class SpecialtyServiceImpl implements ISpecialtyService {

	@Autowired
	private ISpecialtyDAO dao;

	@Override
	public Specialty registrar(Specialty obj) {
		return dao.save(obj);
	}

	@Override
	public Specialty modificar(Specialty obj) {
		return dao.save(obj);
	}

	@Override
	public List<Specialty> listar() {
		return dao.findAll();
	}

	@Override
	public Specialty leer(Integer id) {
		Optional<Specialty> op = dao.findById(id);
		return op.isPresent() ? op.get() : new Specialty();
	}

	@Override
	public void eliminar(Integer id) {
		dao.deleteById(id);
	}

}
