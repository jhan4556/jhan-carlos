package com.app.hospital.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.hospital.dao.IPatientDAO;
import com.app.hospital.model.Patient;
import com.app.hospital.service.IPatientService;

@Service
public class PatientServiceImpl implements IPatientService {

	@Autowired
	private IPatientDAO dao;

	@Override
	public Patient registrar(Patient obj) {
		return dao.save(obj);
	}

	@Override
	public Patient modificar(Patient obj) {
		return dao.save(obj);
	}

	@Override
	public List<Patient> listar() {
		return dao.findAll();
	}

	@Override
	public Patient leer(Integer id) {
		Optional<Patient> op = dao.findById(id);
		return op.isPresent() ? op.get() : new Patient();
	}

	@Override
	public void eliminar(Integer id) {
		dao.deleteById(id);
	}

}
