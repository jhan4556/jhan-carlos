package com.app.hospital.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Historia DoctorXPaciente")
@Entity
@Table(name = "history")
public class History {

	@Id
	@Column(name = "idHistory")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idHistory;

	@ManyToOne
	@JoinColumn(name = "idDoctor", nullable = false, foreignKey = @ForeignKey(name = "id_history_doctor"))
	private Doctor doctor;

	@ManyToOne
	@JoinColumn(name = "idPatient", nullable = false, foreignKey = @ForeignKey(name = "id_history_patient"))
	private Patient patient;

	@Column(name = "calification", columnDefinition = "decimal(3,2)")
	private Double calification;

	@Column(name = "comment", length = 100)
	private String comment;

	@Column(name = "rating", columnDefinition = "decimal(3,2)")
	private Double rating;

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Double getCalification() {
		return calification;
	}

	public void setCalification(Double calification) {
		this.calification = calification;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getIdHistory() {
		return idHistory;
	}

	public void setIdHistory(Integer idHistory) {
		this.idHistory = idHistory;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((calification == null) ? 0 : calification.hashCode());
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + ((doctor == null) ? 0 : doctor.hashCode());
		result = prime * result + ((idHistory == null) ? 0 : idHistory.hashCode());
		result = prime * result + ((patient == null) ? 0 : patient.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		History other = (History) obj;
		if (calification == null) {
			if (other.calification != null)
				return false;
		} else if (!calification.equals(other.calification))
			return false;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (doctor == null) {
			if (other.doctor != null)
				return false;
		} else if (!doctor.equals(other.doctor))
			return false;
		if (idHistory == null) {
			if (other.idHistory != null)
				return false;
		} else if (!idHistory.equals(other.idHistory))
			return false;
		if (patient == null) {
			if (other.patient != null)
				return false;
		} else if (!patient.equals(other.patient))
			return false;
		return true;
	}

}
