package com.app.hospital.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Especialidad M�dica")
@Entity
@Table(name = "specialty")
public class Specialty {
	
	@Id
	@Column(name = "idSpecialty")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idSpecialty;
	
	@Size(min = 3, message = "[name] Minimo 3 caracteres")
	@Column(name = "name", length = 60)
	private String name;

	public Integer getIdSpecialty() {
		return idSpecialty;
	}

	public void setIdSpecialty(Integer idSpecialty) {
		this.idSpecialty = idSpecialty;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSpecialty == null) ? 0 : idSpecialty.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Specialty other = (Specialty) obj;
		if (idSpecialty == null) {
			if (other.idSpecialty != null)
				return false;
		} else if (!idSpecialty.equals(other.idSpecialty))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
