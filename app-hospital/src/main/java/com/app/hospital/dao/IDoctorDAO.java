package com.app.hospital.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.hospital.model.Doctor;

public interface IDoctorDAO extends JpaRepository<Doctor, Integer> {

}
