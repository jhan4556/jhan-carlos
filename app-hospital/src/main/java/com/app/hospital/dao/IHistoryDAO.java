package com.app.hospital.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.hospital.model.History;

public interface IHistoryDAO extends JpaRepository<History, Integer>{

}
