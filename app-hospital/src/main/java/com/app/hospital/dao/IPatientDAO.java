package com.app.hospital.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.hospital.model.Patient;

public interface IPatientDAO extends JpaRepository<Patient, Integer> {

}
