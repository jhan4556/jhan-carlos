package com.app.hospital.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.hospital.model.City;

public interface ICityDAO extends JpaRepository<City, Integer>{

}
