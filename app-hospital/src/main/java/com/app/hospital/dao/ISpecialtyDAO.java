package com.app.hospital.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.hospital.model.Specialty;

public interface ISpecialtyDAO extends JpaRepository<Specialty, Integer> {

}
